import I18n from '../i18n/i18n';
import { MODAL } from '../actions/ActionTypes';
import { fetchFromServer, uploadToServer, checkResponseError } from "../utils/utils";
import RNFetchBlob from "rn-fetch-blob";// Convert file into base64

/**
 * Show the error modal with the given message
 * @param {string} title 
 * @param {string} text 
 */
export function showErrorModal( title = null, text = null){
    return {
        type: MODAL.OPENERROR,
        payload: {
            errorTitle: title,
            errorText: text,
        },
    };
}

/** Hide the modal, don't reset the given values */
export function closeErrorModal(){
    return {
        type: MODAL.CLOSEERROR,
    };
}

/** Hide the modal and reset it's values */
export function resetErrorModal(){
    return {
        type: MODAL.RESETERROR,
    };
}

export function openLoginModal(onLoginDone = null) {
    return {
        type: MODAL.OPENLOGIN,
        onLoginDone
    }
}

export function closeLoginModal() {
    return {
        type: MODAL.CLOSELOGIN,
    }
}

export function openLoadingModal(modalTitle, modalText) {
    return {
        type: MODAL.OPENLOADING,
        modalTitle, modalText
    }
}

export function closeLoadingModal() {
    return {
        type: MODAL.CLOSELOADING
    }
}

export function openForceSyncModal(forceSyncTitle = null, forceSyncText = null, onSyncPress = null) {
    return {
        type: MODAL.OPENFORCESYNC,
        payload: {forceSyncTitle, forceSyncText, onSyncPress},
    }
}

export function closeForceSyncModal() {
    return {
        type: MODAL.CLOSEFORCESYNC,
    }
}

/**
 * Export an existing tree to a zip file
 * @param {any} tree the tree that we want to export */
export function openTreeExportModal( tree ){
    return function( dispatch, getState) {
        return fetchFromServer(
            getState().authenticationReducer.serverUrl,
            `custom_trees/${tree.id}/export`,
            'POST',
            null,
            null,
            getState().authenticationReducer.signinCredentials
        )
        .then( checkResponseError(dispatch, getState, I18n.t("error.tree.export")) )
        .then( (response) => {
            dispatch({
              type: MODAL.OPENJOBSTATUS,
              payload: response.data,
            });
        })
        .catch(error => {
            console.log("startTreeExport error: ", error);
            dispatch(showErrorModal( I18n.t("error.tree.export"), error.message || JSON.stringify(error)));
        });
    }
}

/**
 * Import an existing tree to a zip file
 * @param {any} tree the tree that we want to export */
export function openTreeImportModal( filePath ){
    const files = [{
      name: 'file',
      filename: 'tree.zip',
      data: RNFetchBlob.wrap(filePath),
    }];

    return function( dispatch, getState) {
        return uploadToServer(
            getState().authenticationReducer.serverUrl,
            'custom_trees/import',
            'POST',
            { 'Content-Type' : 'multipart/form-data' },
            files,
            getState().authenticationReducer.signinCredentials
        )
        .then( checkResponseError(dispatch, getState, I18n.t("error.tree.import")) )
        .then( (response) => {
            dispatch({
              type: MODAL.OPENJOBSTATUS,
              payload: response.data,
            });
        })
        .catch(error => {
            console.log("startTreeImport error: ", error);
            dispatch(showErrorModal( I18n.t("error.tree.import"), error.message || JSON.stringify(error)));
        });
    }
}

export function pollJobStatusModal() {
    return function( dispatch, getState) {
        const { jobStatus } = getState().modalReducer;
        if (!jobStatus) return;
        if (jobStatus.status === 'complete') return true;

        return fetchFromServer(
            getState().authenticationReducer.serverUrl,
            `job_statuses/${jobStatus.id}`,
            'GET',
            null,
            null,
            getState().authenticationReducer.signinCredentials
        )
        .then( checkResponseError(dispatch, getState, I18n.t("error.jobstatus.poll")) )
        .then( (response) => {
            dispatch({
              type: MODAL.UPDATEJOBSTATUS,
              payload: response.data,
            });
        })
        .catch(error => {
            console.log("pollJobStatus error: ", error);
            dispatch(showErrorModal( I18n.t("error.jobstatus.poll"), error.message || JSON.stringify(error)));
        });
    }
}

export function closeJobStatusModal() {
    return {
        type: MODAL.CLOSEJOBSTATUS,
    }
}

