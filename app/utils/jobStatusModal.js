import React, {Component} from 'react';
import { View, StyleSheet, Linking } from 'react-native';
import { Content, Card, Button, H3, Text, Spinner } from 'native-base';
import { connect } from 'react-redux';

// components
import I18n from '../i18n/i18n';

// third party
import Modal from "react-native-modalbox";

// actions
import { pollJobStatusModal, closeJobStatusModal } from "../actions/ModalAction";

/**
 * Modal used to force user to synchronize.
 * It's inserted in the Root component, and any screen in the app
 * can make this visible.
 * The graphic is created by following the Material Design guidelines.
 */
class JobStatusModal extends Component {
    componentDidMount() {
      this._interval = setInterval(() => {
        if (!this.props.jobStatus || !this.props.showJobStatusModal) return;
        if (this.props.jobStatus.status !== 'complete') {
          this.props.pollJobStatusModal();
        }
      }, 2000);
    }

    componentWillUnmount() {
      if (this._interval !== null) clearInterval(this._interval);
      this._interval = null;
    }

    /*
    componentDidUpdate(prevProps) {
      if (!this.props.showJobStatusModal) return;
      if (!prevProps.jobStatus || !this.props.jobStatus) return;
      if (prevProps.jobStatus.id !== this.props.jobStatus.id) return;
      if (prevProps.jobStatus.status === this.props.jobStatus.status) return;

      // status change
      this.onJobStatusChanged(prevProps.jobStatus.status);
    }

    onJobStatusChanged(prevStatus) {
      if (this.props.jobStatus.status !== 'complete') return;

      switch (this.props.jobStatus.job_type) {
        case 'DownloadTreeJob':
          break;

        case 'UploadTreeJob':
          // UserLibraryScreen.list.tryResetList()
          break;
      }
    }
    */

    onActionPress = () => {
      switch (this.props.jobStatus.job_type) {
        case 'DownloadTreeJob':
          Linking.openURL(`${this.props.serverUrl}/${this.props.jobStatus.result}`)
            .then(() => this.props.closeJobStatusModal())
            .catch(err => console.error("error downloading", err));
          break;

        case 'UploadTreeJob':
          this.props.closeJobStatusModal();
          break;
      }
    }

    actionButton() {
      const { jobStatus } = this.props;
      switch (jobStatus.job_type) {
        case 'DownloadTreeJob':
          return (
            <Button transparent primary
                disabled={jobStatus.status !== 'complete'}
                style={styles.modalButtonsContainer}
                onPress={this.onActionPress}>
                <Text uppercase>{this.jobT('action')}</Text>
            </Button>
          );

        case 'UploadTreeJob':
          return (
            <Button transparent primary
                style={styles.modalButtonsContainer}
                onPress={this.onActionPress}>
                <Text uppercase>{this.jobT('action')}</Text>
            </Button>
          );
          break;
      }
    }

    jobT(field) {
      return I18n.t(['job', this.props.jobStatus.job_type, field]);
    }

    render() {
        const { showJobStatusModal, closeJobStatusModal, jobStatus } = this.props;
        if (!showJobStatusModal) return null;
        return (
            <Modal
                style={styles.modal}
                backdropPressToClose={true}
                swipeToClose={false}
                backButtonClose={true}
                visible={showJobStatusModal}
                isOpen={showJobStatusModal}
                coverScreen={true}
                onClosed={closeJobStatusModal}>
                <Content style={styles.modalContainer}>
                    <Card>
                        <View style={styles.modalContentContainer}>
                            <H3 style={{marginBottom: 20,}} numberOfLines={2}>{this.jobT('title')}</H3>

                            {(jobStatus.status !== 'complete' && jobStatus.status !== 'failed') && <Spinner />}

                            <Text uppercase style={{marginVertical: 2}} numberOfLines={5}>{this.jobT(jobStatus.status)}</Text>
                        </View>

                        <View style={styles.modalButtonsContentContainer}>
                          {this.actionButton()}
                        </View>
                    </Card>
                </Content>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    
    modal: {
        maxHeight: 400,
        backgroundColor: "transparent",
    },

    modalContainer : {
        margin: 40,
        backgroundColor: "transparent",
    },

    modalContentContainer: {
        margin: 24,
    },

    modalButtonsContentContainer: {
        flexDirection: "row",
        justifyContent: "flex-end",
    },

    modalButtonsContainer: {
        margin: 8,
    },
});

function mapStateToProps (state) {
    return {
        showJobStatusModal: state.modalReducer.showJobStatusModal,
        jobStatus: state.modalReducer.jobStatus,
        serverUrl: state.authenticationReducer.serverUrl,
    }   
}

function mapDispatchToProps(dispatch) {
    return {
        closeJobStatusModal: () => dispatch(closeJobStatusModal()),
        pollJobStatusModal: () => dispatch(pollJobStatusModal()),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(JobStatusModal);
