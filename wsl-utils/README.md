these are tools to make working with react-native in WSL easier.

`proxy.bat`
===========

Used to expose some ports on the WSL interfaces to the local network by binding to 0.0.0.0 on the windows host system.

- 8081: react-native bundler/debug socket. This allows running react-native-debugger (version 0.10) natively on Windows.
- 8097: react debug socket. This allows running react-native-debugger (version 0.10) natively on Windows.
- 8001: don't remember if this is really necesssary
- 3001: sayeye-server API port, to allow access from LAN.

Before running, adjust:

* `connectaddress=172.21.191.7` to the local IP of the WSL network (found using `ip a` inside an instance)


`bind-usb.bat` / `unbind-usb.bat`
=================================

Used to share a USB device from Windows to a WSL Linux system.

These can be used from an *Administrator Command Prompt* on the Windows side.
Before running, adjust:

* `-b 5-2` (multiple places in both files) to the USB BUSID of the phone (found using `usbipd.exe wsl list`)
* `-d sayeye-mobile-dev` the WSL distribution name of the distro to share it with

`bind-usb.bat` often has to be run repeatedly until it works (as confirmed by `STATE attached` output).
Once the USB device is attached to the WSL instance, `adb devices` should list it inside the WSL shell.

`adb-reverse.sh`
================

Can be used inside a WSL system to set up adb reverse-port forwarding for:

- 8081: give access to the react bundler to load code into debug builds
- 8097: react debugger port
